INTRODUCTION
Easy Clean is a perfect multi-region base theme for who loves to build up a website from scratch.

FEATURES
- Multiple regions
- Reset CSS
- HTML5 support
- Modernizr

REGIONS
Outside Top
Header Top
Header
Header Bottom
Preface Top
Preface Bottom
Sidebar Left
Sidebar Right
Content Top
Content
Content Bottom
Postscript Top
Postscript Bottom
Footer Top
Footer
Footer Bottom
Outside Bottom

